import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

//引入组件
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
import Users from '../components/Users.vue'
import Rights from '../components/power/Rights.vue'
import Roles from '../components/power/Roles.vue'
import Cate from '../components/goods/Cate.vue'
import Goods from '../components/goods/Goods.vue'
import Params from '../components/goods/Params.vue'
import Add from '../components/goods/Add.vue'
import Order from '../components/order/Order.vue'
import Report from '../components/report/Report';
//创建路由
const router = new VueRouter({
  routes: [
    {
      //设置路由重定向
      path: '/',
      //当打开是/路径时就路由重定向到/login地址
      redirect: '/login'
    },
    {
      path: '/login',
      component: Login,
    },
    {
      path: '/home',
      component: Home,
      //给home路由重定向
      redirect: 'welcome',
      children: [
        {
          path: '/welcome',
          component: Welcome,
        },
        {
          path: '/users',
          component: Users
        },
        {
          path: '/rights',
          component: Rights
        },
        {
          path: '/roles',
          component: Roles
        },
        {
          path: '/categories',
          component: Cate
        },
        {
          path: '/goods',
          component: Goods,
        },
        {
          path: '/params',
          component: Params
        },
        {
          path: '/goods/add',
          component: Add,
        },
        {
          path: '/orders',
          component: Order,
        },
        {
          path: '/reports',
          component: Report,
        }
      ]
    }
  ]
})
//设置全局前置路由守卫 设置没有token时不能直接通过url登录
router.beforeEach((to, from, next) => {
  //如果用户访问的登录页 直接放行
  if (to.path === '/login') return next()

  //从sessionstrage中获取到保存的token值
  const tokenStr = sessionStorage.getItem('token')
  //如果没有token的值就返回到登录页面 有的话放行
  if (!tokenStr) return next('/login')
  next()
})
export default router
