import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import TreeTable from 'vue-table-with-tree-grid'
//导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
//导入富文本编辑的样式
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
//引入axios
import axios from 'axios'
//设置一个axios的根路径 配置请求的根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'

//axios请求拦截器

//为接口发放一个令牌当能调用登录时的token时登陆后的其他接口请求才能使用
//如果拿不到token令牌那么其他请求就不会被请求
axios.interceptors.request.use(config => {
  // console.log(config);
  //为config的请求头获取一个登陆时创建的令牌
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // 最后必须返回config
  return config
})
//把axios挂载到vue的原型上能全局使用
Vue.prototype.$http = axios
//导入全局样式表
import './assets/css/global.css'
Vue.config.productionTip = false
// 使用注册TreeTable插件
Vue.component('tree-table', TreeTable)
//配置一个全局的时间过滤器
Vue.filter('dateFormat', function (originVal) {
  const dt = new Date(originVal)
  const y = dt.getFullYear()
  const m = (dt.getMonth() + 1 + '').padStart(2, '0')//这里是如果月份不足两位在数字前面加0
  const d = (dt.getDate() + '').padStart(2, '0')
  const hh = (dt.getHours() + '').padStart(2, '0')
  const mm = (dt.getMinutes() + '').padStart(2, '0')
  const ss = (dt.getSeconds() + '').padStart(2, '0')
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})
//使用注册VueQuillEditor插件
Vue.use(VueQuillEditor, /* { default global options } */)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
